function varargout = mTrack(varargin)
% MTRACK MATLAB code for mTrack.fig
%      MTRACK, by itself, creates a new MTRACK or raises the existing
%      singleton*.
%
%      H = MTRACK returns the handle to a new MTRACK or the handle to
%      the existing singleton*.
%
%      MTRACK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MTRACK.M with the given input arguments.
%
%      MTRACK('Property','Value',...) creates a new MTRACK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mTrack_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mTrack_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mTrack

% Last Modified by GUIDE v2.5 12-Sep-2017 12:43:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mTrack_OpeningFcn, ...
                   'gui_OutputFcn',  @mTrack_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    warning('off')
    gui_mainfcn(gui_State, varargin{:});
    warning('on')
end
% End initialization code - DO NOT EDIT

% --- Executes just before mTrack is made visible.
function mTrack_OpeningFcn(hObject, ~, handles, varargin)
warning('off','MATLAB:hg:UIControlSliderStepValueDifference')
warning('off','MATLAB:nargchk:deprecated')
% Choose default command line output for mTrack
handles.output = hObject;

% Add folders
addpath([pwd '\Functions'])
addpath([pwd '\Resources'])
Settings = load('Settings.mat'); % CHANGE TO DEFAULT 

% Load Video
Settings = Settings.Settings;
[Data.FileName,Data.PathName]= uigetfile(Settings.LoadFormats,'Select Video');
Data.Video = VideoReader([Data.PathName Data.FileName]);

% Update GUI
set(handles.ODthreshold,'String',Settings.ODthreshold)
set(handles.ODnumframes,'String',Settings.ODnumframes)
set(handles.TRpeakthreshold,'String',Settings.TRpeakthreshold)
set(handles.TRsilhouettedilationsize,'String',Settings.TRsilhouettedilationsize)
set(handles.CTRLstopthreshold,'String',Settings.CTRLstopthreshold)
handles.AxesSwitch = 'Tracking';
handles.autoplay = 0;
set(handles.Autoplay,'Value',handles.autoplay);
switch(Settings.ODmethod)
    case 'Average'
        set(handles.ODaverage,'Value',1)
        set(handles.ODzeropixel,'Value',0)
    case 'ZeroPixel'
        set(handles.ODaverage,'Value',0)
        set(handles.ODzeropixel,'Value',1)
end
axes(handles.axes1)

handles.Settings.ExportVideo = 0;
Logo = imread('Logo.png');
imagesc(Logo,'Parent',handles.axes1)
axis 'off'
% Update handles structure
handles.Data = Data;
handles.Settings = Settings;

guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = mTrack_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% OBJECT DETECTION
function ObjectDetection_Callback(hObject, ~, handles) %#ok<*DEFNU>
Data = handles.Data;
Data.Objects = DetectObjects(Data,handles.Settings);
Data.Output.Objects = Data.Objects;
% Show results
figure(1)
clf
imshow(Data.Objects);
title('Detected Objects')

% Save data to GUI
handles.Data = Data;
guidata(hObject,handles)
function ODaverage_Callback(hObject, ~, handles)
% Set Object Detection method to 'Average', update GUI
Settings = handles.Settings;
if get(hObject,'Value')
    Settings.ODmethod = 'Average';
    set(handles.ODzeropixel,'Value',0)
end
handles.Settings = Settings;
guidata(hObject,handles)    
function ODzeropixel_Callback(hObject, ~, handles)
% Set Object Detection method to 'Zero Pixel', update GUI
Settings = handles.Settings;
if get(hObject,'Value')
    Settings.ODmethod = 'ZeroPixel';
    set(handles.ODaverage,'Value',0)
end
handles.Settings = Settings;
guidata(hObject,handles)
function ODthreshold_Callback(hObject, ~, handles)
% Set Object detection threshold
Settings = handles.Settings;
Settings.ODthreshold = str2double(get(hObject,'String'));
handles.Settings = Settings;
guidata(hObject,handles)
function ODnumframes_Callback(hObject, ~, handles)
Settings = handles.Settings;
Settings.ODnumframes = str2double(get(hObject,'String'));
handles.Settings = Settings;
guidata(hObject,handles)

% TRACKING
function SF_Callback(hObject, ~, handles)
Slider(hObject,handles);
function TRpeakthreshold_Callback(hObject, ~, handles)
handles.Settings.TRpeakthreshold = str2double(get(hObject,'String'));
guidata(hObject,handles)
function TRsilhouettedilationsize_Callback(hObject, ~, handles)
handles.Settings.TRsilhouettedilationsize = ...
    str2double(get(hObject,'String'));
guidata(hObject,handles)
function CTRLstopthreshold_Callback(hObject, ~, handles)
handles.Settings.CTRLstopthreshold = str2double(get(hObject,'String'));
guidata(hObject,handles)
function TrackVideo_Callback(hObject, ~, handles)
handles.Data = TrackVideo(handles.Data,handles.Settings);
guidata(hObject,handles)
function ODthreshold_CreateFcn(hObject, ~, ~)
% hObject    handle to ODthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ODnumframes_CreateFcn(hObject, ~, ~)
% hObject    handle to ODnumframes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function SF_CreateFcn(hObject, ~, ~)
% hObject    handle to SF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
function TRpeakthreshold_CreateFcn(hObject, ~, ~)
% hObject    handle to TRpeakthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function TRsilhouettedilationsize_CreateFcn(hObject, ~, ~)
% hObject    handle to TRsilhouettedilationsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function CTRLstopthreshold_CreateFcn(hObject, ~, ~)
% hObject    handle to CTRLstopthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function LoadResults_Callback(hObject, ~, handles)
[handles.Data.FileName,handles.Data.PathName] = uigetfile('Get results');
addpath(handles.Data.PathName)
load(handles.Data.FileName);
vidname1 = [handles.Data.FileName(1:end-16) '.avi'];
vidname2 = [handles.Data.FileName(1:end-16) '.mp4'];
try
    handles.Data.Video = VideoReader([handles.Data.PathName vidname1]);
    handles.Data.FileName = vidname1;
catch
    handles.Data.Video = VideoReader([handles.Data.PathName vidname2]);
    handles.Data.FileName = vidname2;
end

handles.Data.Output = Output;
guidata(hObject,handles)

% SAVE SETTINGS
function SaveSettings_Callback(~, ~, handles)
Settings = handles.Settings;
save([pwd '\Resources\Settings'],'Settings');
disp('SETTINGS SAVED')
%%%%%%%%%%%% MENUBAR

% FILE
function File_Callback(hObject, ~, ~)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --------------------------------------------------------------------

function LoadVideo_Callback(hObject, ~, handles)
Settings = handles.Settings;
[handles.Data.FileName,handles.Data.PathName]= uigetfile({Settings.LoadFormats},'Select Video');
handles.Data.Video = VideoReader([handles.Data.PathName handles.Data.FileName]);
guidata(hObject,handles)

% HELP MENU
function MNhelp_Callback(~, ~, ~)
% hObject    handle to MNhelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
function MNinstructions_Callback(~, ~, ~)
winopen Quickstart.pdf
function AdvancedDescription_Callback(~, ~, ~)
winopen 'Advanced details.pdf'
function About_Callback(~, ~, ~)
winopen 'About.pdf'

function TrackFrame_Callback(hObject, ~, handles)
handles.AxesSwitch = 'Tracking';
Slider(hObject,handles);
guidata(hObject,handles)
function PlayResults_Callback(hObject, ~, handles)

handles.AxesSwitch = 'Replay';
guidata(hObject,handles)
Slider(hObject,handles);

function Autoplay_Callback(hObject, ~, handles)
handles.autoplay = get(hObject,'Value');
guidata(hObject,handles);


% MANUAL TRACKING
function TrackNose_Callback(hObject, ~, handles)
handles.Data = TrackNose(handles.Data,handles.Settings);
guidata(hObject,handles)
function ManualLabelling_Callback(hObject, ~, handles)
Data = handles.Data;
Data.Output.ManTraces = ManualLinking(handles.Data,handles.axes1);
Output = Data.Output;
name = [Data.PathName Data.FileName(1:end-4) '_Annotations'];
if exist(name,'file')
    a = inputdlg('Do you want to overwrite existing file? (y/n)');
    
    while any(class(a{1}) ~= 'char')||  any(size(a{1}) ~= [1,1])
         a = inputdlg('Do you want to overwrite existing file? (y/n)');
    end
    if a{1} == 'y'
        save(name,'Output') 
    end
    
else
    save(name,'Output') 
end
handles.Data = Data;
guidata(hObject,handles)


function ExportVideo_Callback(hObject, ~, handles)
handles.Settings.ExportVideo = get(hObject,'Value');

guidata(hObject,handles)


function AnalyseResults_Callback(hObject, eventdata, handles)
handles.Data = AnalyseResults(handles.Data,handles.Settings);
guidata(hObject,handles)
