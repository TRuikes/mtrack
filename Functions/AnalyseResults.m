function Data = AnalyseResults(Data,Settings)

%%
if isfield(Data.Output,'ManTraces')
    
    Traces = Data.Output.ManTraces;
    nLabels = 0;
    Ref =[0 1];
    nFrames = size(Traces,2);
    Xaxis = 1:nFrames;
    
    
    for FrameNR = 1:nFrames
        if size(Traces{FrameNR},2) > nLabels
            nLabels = size(Traces{FrameNR},2);
        end
    end
    
    nCols = 3;
    nRows  = ceil(nLabels/nCols);
    ColNR = 1;
    RowNR = 1;
    cc = hsv(nLabels);
    
    
    
    %% Find Lengts of traces
    
    figure(1)
    hold on
    for LabelNR = 1:nLabels
        for FrameNR = 1:nFrames
            if ~isempty(Traces{FrameNR})
                if LabelNR <= size(Traces{FrameNR},2) && ...
                        ~isempty(Traces{FrameNR}{LabelNR})
                    Lengths{LabelNR}(FrameNR) = size(Traces{FrameNR}{LabelNR},1);
                else
                    Lengths{LabelNR}(FrameNR) = NaN;
                end
            else
                Lengths{LabelNR}(FrameNR) = NaN;
            end
        end
        IDX = 1:size(Lengths{LabelNR},2);
        plot(Xaxis(IDX),Lengths{LabelNR}(IDX),'color',cc(LabelNR,:))
        TrackedIDX{LabelNR} = find(~isnan(Lengths{LabelNR}))
    end
    
    Data.Output.Analysis.Lengths = Lengths;
    %% Find angle of traces
    figure(2)
    clf
    for LabelNR = 1:nLabels
        for FrameNR = 1:nFrames
            if ~isempty(Traces{FrameNR})
                if LabelNR <= size(Traces{FrameNR},2) && ...
                        ~isempty(Traces{FrameNR}{LabelNR})
                    
                    Trace = Traces{FrameNR}{LabelNR};
                    if size(Trace,1) >= 8
                        Vec = Trace(8,1:2) -  Trace(2,1:2);
                        Angle = atan2d(Ref(1),Ref(2)) - atan2d(Vec(1),Vec(2));
                        if Angle < 0
                            Angle = 360+Angle;
                        end
                        
                        Angles{LabelNR}(FrameNR) = Angle;
                    else
                        Angles{LabelNR}(FrameNR) = NaN;
                    end
                else
                    Angles{LabelNR}(FrameNR) = NaN;
                end
            else
                Angles{LabelNR}(FrameNR) = NaN;
            end
            
        end
        
        
        % Filter Angles
        fitaxis = 1:nFrames;
        AnglesFilt{LabelNR} = Angles{LabelNR};
        dA = diff(AnglesFilt{LabelNR}(IDX));
        FalseIDX = find( dA < -20 | dA > 20);
        TrueIDX = find(dA > -20 & dA < 20);
        fitaxis(FalseIDX) = NaN;
        fitaxis(find(isnan(AnglesFilt{LabelNR}))) = NaN;
        fitaxis = fitaxis(~isnan(fitaxis));
        fitaxis(end+1) = nFrames;
        dIDX = diff(fitaxis);
        gIDX = find(dIDX>10);
        
        for i = 1:length(gIDX)
            saxis(i,1) = fitaxis(gIDX(i));
            saxis(i,2) = fitaxis(gIDX(i)+1);
            
        end
        fitaxis = 1:nFrames;
        for i = 1:size(saxis,1)
            fitaxis(saxis(i,1):saxis(i,2)) = NaN;
        end
       fitaxis = fitaxis(~isnan(fitaxis));
        
       
     
            
        %AnglesFilt{LabelNR} = spline(TrueIDX,AnglesFilt{LabelNR}(...
        %    TrueIDX),fitaxis);
        %AnglesFilt{LabelNR} = hampel(AnglesFilt{LabelNR},1,1);
        %AnglesFilt{LabelNR} = medfilt1(AnglesFilt{LabelNR},2);
        figure(2)
        %subplot(nRows,nCols,LabelNR)
        hold on
        IDX = 1:size(Angles{LabelNR},2);
        %plot(Xaxis(IDX),Angles{LabelNR}(IDX),'color','k')
        plot(Xaxis(IDX),AnglesFilt{LabelNR}(IDX),'color',cc(LabelNR,:))
        ylim([100 250])
        title(num2str(LabelNR))
        
        
       
    end
    Data.Output.Analysis.Angles = AnglesFilt;
    
    
    
    %% Detect touch
    [Objects(:,1),Objects(:,2)]= ind2sub(size(Data.Output.Objects ), find(Data.Objects));
     for LabelNR = 1:nLabels
        for FrameNR = 1:nFrames
            disp(FrameNR)
            if ~isempty(Traces{FrameNR})
                if LabelNR <= size(Traces{FrameNR},2) && ...
                        ~isempty(Traces{FrameNR}{LabelNR})
                    Trace  = Traces{FrameNR}{LabelNR};
                    dd = [];
                    for i = 1:size(Trace,1)
                        p = Trace(i,:);
                        dd(i) =  min(sqrt(sum((Objects - p).^2,2)));
                    end
                    
                    if min(dd) < 10
                        id = find(dd == min(dd),1,'first');
                        Touch{LabelNR}(FrameNR,1:2)= Trace(id,1:2);
                    else
                        Touch{LabelNR}(FrameNR,1:2) = [NaN,NaN];
                    end
                    
                end
            end
        end
     end
     
     
     Data.Output.Analysis.Touch = Touch;
else
    disp('NO LABELLED TRACES')
end