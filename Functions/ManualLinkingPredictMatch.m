function [mTraces,matchlog,untraced] = ManualLinkingPredictMatch...
    (mTraces,Traces,Data,FrameNR)
method = 2;
switch(method)
    case 1 % Use correlation between traces
        %%
        Ref = [1 0];
        % Find number of previous labelled frames
        if FrameNR < 11
            nPrev = FrameNR - 1;
        else
            nPrev = 10;
        end
        
        
        if nPrev > 0
            nMarked = size(mTraces{FrameNR-1},2);
            nTraces = size(Traces{FrameNR},2);
            for i = 1:nMarked
                for j = 1:nTraces
                    A = mTraces{FrameNR-1}{i};
                    B = Traces{FrameNR}{j};
                    dL = abs(size(A,1)-size(B,1));
                    
                    if ~isempty(A) && ~isempty(B)
                        if size(A,1) > size(B,1)
                            x(i,j) = corr2(A(1:size(B,1),:),B);
                        elseif size(B,1) >= size(A,1)
                            x(i,j) = corr2(A,B(1:size(A,1),:));
                        end
                        if dL > 10 && dL < 20
                            x(i,j) = x(i,j) - 0.5;
                        elseif dL >= 20
                            x(i,j) = x(i,j) - 0.5;
                        end
                    else
                        x(i,j) = NaN;
                    end
                end
            end
            matchlog = [];
            x(x<0.7) = NaN;
            while any(any(~isnan(x)))
                [LabelNR,TraceNR] = find(x == max(max(x)),1,'first');
                mTraces{FrameNR}{LabelNR} = Traces{FrameNR}{TraceNR};
                x(LabelNR,:) = NaN;
                x(:,TraceNR) = NaN;
                matchlog(LabelNR) = TraceNR;
            end
            
            untraced = 1:nTraces;
            for i = 1:length(matchlog)
                untraced(untraced == matchlog(i)) = NaN;
            end
            untraced = untraced(~isnan(untraced));
            
        elseif nPrev == 0
            
            
            
            
            % Assign each Trace to a label
            nLabels = size(Traces{FrameNR},2);
            for IDX = 1:nLabels
                mTraces{FrameNR}{IDX}  = Traces{FrameNR}{IDX};
            end
            
            
            
        end
        
    case 2 % Assign label based on position
        minlength = str2num(Data.minlength);
        for ID = 1:size(Traces{FrameNR},2)
            if size(Traces{FrameNR}{ID},1) > minlength
                RotTrace{ID} = RotateTrace(Traces{FrameNR}{ID},Data,FrameNR);
                
            end
            
        end
        %{
        For visualization purposes
        figure(1)
        clf
        hold on
        cc = hsv(20);
        for i = 1:size(RotTrace,2)
            plot(RotTrace{i}(:,2),RotTrace{i}(:,1),'color',cc(i,:))
        end
        
        %}
        Left = 999; Right = 999;
        if ~exist('RotTrace','var')
            RotTrace = [];
            Label = [];
            matchlog = [];
        end
        for ID = 1:size(RotTrace,2) % Sort on L/R side and assign distance
            if ~isempty(RotTrace{ID}) && RotTrace{ID}(1,2) >= 0
                Left(ID) = sqrt( (sum(RotTrace{ID}(1,1:2).^2)));
            elseif  ~isempty(RotTrace{ID}) && RotTrace{ID}(1,2) < 0
                Right(ID) = sqrt( (sum(RotTrace{ID}(1,1:2).^2)));
            end
        end
        Left(Left == 0) = 999;
        Right(Right == 0) = 999;
        labelnr = 1;
        % Assign labels
        while any(Left ~= 999) & labelnr < 5
            id = find(Left == min(Left));
            Label(labelnr) = id;
            Left(id) = 999;
            labelnr = labelnr+1;
        end
        
        labelnr = 5;
        while any(Right ~= 999) & labelnr < 9
            id = find(Right == min(Right));
            Label(labelnr) = id;
            Right(id) = 999;
            labelnr = labelnr +1;
        end
        
        
        for i = 1:length(Label)
            if Label(i) ~= 0
                mTraces{FrameNR}{i} = Traces{FrameNR}{Label(i)};
                matchlog(i) = Label(i);
            end
        end
        
        nTraces = size(Traces{FrameNR},2);
        untraced = 1:nTraces;
        for i = 1:length(matchlog)
            untraced(untraced == matchlog(i)) = NaN;
        end
        untraced = untraced(~isnan(untraced));
        
        
end
