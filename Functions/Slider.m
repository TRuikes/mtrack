function Slider(hObject,handles)

switch(handles.AxesSwitch)
    case 'Tracking'
        if isfield(handles.Data,'Objects')
            axes(handles.axes1)
            
            % Track a single frame
            Data = handles.Data;
            Settings = handles.Settings;
            Video = Data.Video;
            Time = Video.Duration*handles.SF.Value;
            if Time <= 0
                Time = 0.01;
            end
            FrameNR = Time*Video.FrameRate;
            Data.SFframe = LoadFrame(Video,FrameNR);
            Data = TrackFrame(Data,Settings);
            
            % Show results
            imagesc(Data.SFframe,'Parent',handles.axes1)
            axis 'off'
            colormap 'gray'
            hold on
            if ~isempty(Data.Origins)
                scatter(Data.Origins(:,2),Data.Origins(:,1),'r','filled')
                
                
                for TraceNR = 1:size(Data.Traces,2)
                    plot(Data.Traces{TraceNR}(:,2),Data.Traces{TraceNR}(:,1),'r')
                end
            end
            hold off
        elseif ~isfield(handles.Data,'Objects')
            disp('DETECT OBJECTS FIRST')
        end
    case 'Replay'
        
        if handles.Settings.ExportVideo == 1
            vidout = VideoWriter([handles.Data.PathName ...
                handles.Data.FileName(1:end-4)  '_Marked.avi']);
            open(vidout)
        end
        
        if isfield(handles.Data.Output,'ManTraces')
            Traces = handles.Data.Output.ManTraces;
            cc = hsv(9);
            flag = 1;
        elseif isfield(handles.Data.Output,'RawTraces')
            Traces = handles.Data.Output.RawTraces;
            cc = zeros(90,3);
            cc(:,1) = 1;
            flag = 1;
        else
            disp('LOAD TRACES FIRST')
            flag = 0;
        end
        
        if isfield(handles.Data.Output,'Analysis')
            Touch = handles.Data.Output.Analysis.Touch;
            touchflag = 0;
            cc = hsv(size(Touch,2));
        else
            touchflag = 0;
        end
        
        if flag == 1
            axes(handles.axes1)
            Video = handles.Data.Video;
            nframes = floor(Video.Duration*Video.FrameRate);
            Nose = handles.Data.Output.Nose;
            if handles.autoplay == 1
                for FrameNR = 1:nframes
                    disp(hObject.Value)
                    if hObject.Value == 0
                        return
                    end
                    frame = read(Video,FrameNR);
                    imagesc(frame,'Parent',handles.axes1)
                    axis 'off'
                    colormap 'gray'
                    hold on
                    axes(handles.axes1)
                    scatter(Nose(FrameNR,2),Nose(FrameNR,1),'MarkerFaceColor',...
                        'r','MarkerEdgeColor','k')
                    if FrameNR <= size(Traces,2)
                        for TraceNR = 1:size(Traces{FrameNR},2)
                            if ~isempty(Traces{FrameNR}{TraceNR})
                                plot(Traces{FrameNR}{TraceNR}(:,2),Traces{FrameNR}{TraceNR}...
                                    (:,1),'color',cc(TraceNR,:))
                            end
                        end
                    end
                    
                    if touchflag == 1
                        for LabelNR = 1:size(Touch,2)
                            if ~isnan(Touch{LabelNR}(FrameNR,1))
                                PT = Touch{LabelNR}(FrameNR,1:2);
                                scatter(PT(2),PT(1),'MarkerFaceColor'...
                                    ,cc(LabelNR,:),'Marker','x')
                                
                            end
                        end
                    end
                    
                    
                    
                    drawnow
                    hold off
                    if handles.Settings.ExportVideo == 1
                        frameout = getframe;
                        writeVideo(vidout,frameout);
                    end
                end
                
            elseif handles.autoplay == 0
                
                axes(handles.axes1)
                FrameNR = round(get(hObject,'Value')*nframes);
                frame = read(Video,FrameNR);
                imagesc(frame,'Parent',handles.axes1)
                axis 'off'
                colormap 'gray'
                hold on
                axes(handles.axes1)
                scatter(Nose(FrameNR,1),Nose(FrameNR,2),'MarkerFaceColor',...
                    'r','MarkerEdgeColor','k')
                if FrameNR <= size(Traces,2)
                    for TraceNR = 1:size(Traces{FrameNR},2)
                        if ~isempty(Traces{FrameNR}{TraceNR})
                            plot(Traces{FrameNR}{TraceNR}(:,2),Traces{FrameNR}{TraceNR}...
                                (:,1),'color',cc(TraceNR,:))
                        end
                    end
                end
                
                if touchflag == 1 && FrameNR <= size(Touch{1},1)
                    for LabelNR = 1:size(Touch,2)
                        if ~isnan(Touch{LabelNR}(FrameNR,1))
                            PT = Touch{LabelNR}(FrameNR,1:2);
                            scatter(PT(2),PT(1),'MarkerFaceColor'...
                                ,cc(LabelNR,:),'Marker','x')
                            
                        end
                    end
                end
                
                hold off
                drawnow
            end
        elseif ~isfield(handles,'Traces')
            
        end
        
        if handles.Settings.ExportVideo  == 1
            close(vidout)
        end
        
end