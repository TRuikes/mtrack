function RotTrace = RotateTrace(Trace,Data,FrameNR)
%RotTrace = RotateTrace(Trace,Data)
% Correct trace orientation for head movement by translating for head
% position and rotation for headangle
%% Rotation and Translation

% Setup Parameters
Ref = [1,0]; % Align Trace with y-axis
Nose = Data.Output.Nose(FrameNR,:);
Angle = Data.Output.AngleVector(FrameNR,:);

dA = atan2d(Angle(1),Angle(2)) - atan2d(Ref(1),Ref(2)); % Rotation angle

R = [cosd(dA), -sind(dA);sind(dA), cosd(dA)]; % Rotationmatrix

RotTrace = round((R*([Trace(:,1) - Nose(1),Trace(:,2)-Nose(2)])')'); % Corrected Traces
