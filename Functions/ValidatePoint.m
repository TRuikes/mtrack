function flag = ValidatePoint(Data,Settings,Point)
%Flag = ValidatePoint(Data,Settings,Point) validates returns 1 if Point is
% a valid point or 0 If not, based on the requirements that the point is not:
%  -	within a small distance from border
%  -	on an object
%  -	on the mouse
%  -	indistinguishable from noise
%  -	tracked before 
%%
Objects = Data.Objects;
SilhouetteSmall = Data.SilhouetteSmall;
Frame = Data.SFframe;
Tracked = Data.Tracked;

% Controls if Point is a valid point on any whisker.
flag = 1; % Valid point by default

%% The point is outside a speicifc range from border
if Point(1) < Settings.CTRLdistfromborder || ...
        Point(1) > size(Objects,1) - Settings.CTRLdistfromborder || ...
        Point(2) < Settings.CTRLdistfromborder || ...
        Point(2) > size(Objects,2) - Settings.CTRLdistfromborder
    flag = 0;
    %Data.log = 'Border';
end

%% The point lies on an object
if flag == 1 && Objects(Point(1),Point(2)) 
    flag = 0;
   % Data.log = 'Object';
end

%% The point lies on the mouse
if flag == 1 && SilhouetteSmall(Point(1),Point(2))
    flag = 0;
    %Data.log = 'Mouse';
end

%% The point is indistinguishable from noise

IntensityRatio = 0;
if flag == 1 
    
    %%
    
    X1 = Point(1) - 5; if X1 < 1; X1 = 1;end
    X2 = Point(1) + 5; if X2 > size(Objects,1); X2 = Data.Video.Height;end
    
    Y1 = Point(2) - 5; if Y1 < 1; Y1 = 1;end
    Y2 = Point(2) + 5; if Y2 > size(Objects,2); Y2 = Data.Video.Width;end
    
    GridX = X1:X2;
    GridY = Y1:Y2;
    
    
    [mGridX,mGridY] = meshgrid(GridX,GridY);
    mGridX = mGridX(:);
    mGridY = mGridY(:);
    
    GridIDX = sub2ind(size(Objects),mGridX,mGridY);
    GridIDX = GridIDX(Objects(GridIDX) == 0);
    GridIDX = GridIDX(SilhouetteSmall(GridIDX) == 0);
    
    toggle = 1;
    switch(toggle)
        case 1  
            %{
            Diamond(1:3,1) = Point(1)-1:Point(1)+1;
            Diamond(1:3,2) = Point(2);
            Diamond(4:6,1) = Point(1);
            Diamond(4:6,2) = Point(2)-1:Point(2)+1;
            %}
            X1 = Point(1) - 1; if X1 < 1; X1 = 1;end
            X2 = Point(1) + 1; if X2 > Data.Video.Height; X2 = Data.Video.Height;end
            
            Y1 = Point(2) - 1; if Y1 < 1; Y1 = 1;end
            Y2 = Point(2) + 1; if Y2 > Data.Video.Width; Y2 = Data.Video.Width;end
            X = X1:X2;
            Y = Y1:Y2;
            [D1,D2] = meshgrid(X,Y);
            DiaID = [D1(:),D2(:)];
            dind = sub2ind(size(Data.SFframe),DiaID(:,1),DiaID(:,2));
            dind = dind(Data.Objects(dind) == 0);
            
            
        case 2
            % alternate diamond
            X1 = Point(1) - 1; if X1 < 1; X1 = 1;end
            X2 = Point(1) + 1; if X2 > Data.Video.Height; X2 = Data.Video.Height;end
            
            Y1 = Point(2) - 1; if Y1 < 1; Y1 = 1;end
            Y2 = Point(2) + 1; if Y2 > Data.Video.Width; Y2 = Data.Video.Width;end
            [mD1,mD2] = meshgrid(X1:X2,Y1:Y2);
            mD1 = mD1(:);
            mD2 = mD2(:);
            Diamond(:,1) = mD1;
            Diamond(:,2) = mD2;
        case 3
            Diamond = Point;
    end
    %DiamondIDX = sub2ind(size(Objects),Diamond(:,1),Diamond(:,2));
    
    MeanIntensity = sum(Frame(GridIDX))/size(GridIDX,1);
    DiamondIntensity = sum(Frame(dind))/size(dind,1);
    
    IntensityRatio = DiamondIntensity/MeanIntensity;
    
    if IntensityRatio > Settings.CTRLstopthreshold
        flag = 0;
        %Data.log = 'Tip';
    end
  
end


%% The point has allready been tracked
if flag == 1 && Tracked(Point(1),Point(2)) == 1
    flag = 0;
   % Data.log = 'Tracked';
end


