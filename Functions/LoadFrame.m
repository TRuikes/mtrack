function frame = LoadFrame(Video,framenr)
%frame = LoadFrame(Video) loads a frame from Object �Video� and converts it 
% to a suitable output format. Frame format for processing should be a 
% m x n matrix of type double with a grayscale ranging from 0 � 1.
%%

frame = read(Video,framenr);
frame = im2double(frame(:,:,1));
frame = (frame - min(min(frame))) ./ max(max(frame));
h = fspecial('gaussian',10);
frame = imfilter(frame,h);
h = fspecial('laplacian');
f = imfilter(frame,h);
frame = frame - f;
%{
f2 = im2double(rgb2gray(frame));
f3 = f2;

h = fspecial('gaussian',5);
%f3  = imfilter(f3,h);
f2 = imfilter(f2,h);
f2 = adapthisteq(f2,'NumTiles',[2 2]);
h = fspecial('laplacian');
f2 = imfilter(f2,h);
f2 = f2 + ones(size(f2))*abs(min(min(f2)));
f2 = imadjust(f2,[min(min(f2)) max(max(f2))],[0 1]);
f2 = ones(size(f2)) - f2;
f2 = (f3 + f2)/2;
f2 = imadjust(f2,[min(min(f2)) max(max(f2))],[0 1]);
frame = histeq(f2);
%}
