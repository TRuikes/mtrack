function Objects = DetectObjects(Data,Settings)
%Objects = DetectObjects(Data,Settings) applies one of two object detection 
% methods using �Data� And �Settings�. Methods:
% -	Average, take average of all frames in sampleset, binarize the result
% -	ZeroPixel, find all zero pixels in sampleset and set those to zero in 
%   Objects
%%

switch(Settings.ODmethod)
    case 'Average'
        % Find objects in Video by averaging over BAnumframes
        Video = Data.Video;
        nframes = Video.Duration*Video.FrameRate;


        % Sample over interval
        SummedFrames = zeros(Video.Height,Video.Width);
        for FrameNR = 1:floor(nframes/Settings.ODnumframes):nframes         
            frame = LoadFrame(Video,FrameNR); 
            SummedFrames = SummedFrames + frame;
        end
        SummedFrames = SummedFrames./Settings.ODnumframes;

        % Threshold against user defined value
        Objects = zeros(Video.Height,Video.Width);
        Objects(SummedFrames <= Settings.ODthreshold) = 1;
    case 'ZeroPixel'
       % Find object by setting any pixel that becomes below threshold to
       % zero
       Video = Data.Video;     
       nframes = round(Video.Duration*Video.FrameRate);
       % Sample over interval
       Objects = ones(Video.Height,Video.Width);
       for FrameNR = 1:floor(nframes/Settings.ODnumframes):nframes
           frame = LoadFrame(Video,FrameNR);
           Objects(find(frame >= Settings.ODthreshold)) = 0;
       end
end
Objects = imdilate(Objects,strel('diamond',2));
Objects = imfill(Objects,'holes');
   



