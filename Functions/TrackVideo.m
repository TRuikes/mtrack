function Data = TrackVideo(Data,Settings)
%Data = TrackVideo(Data,Settings) tracks a video using �Data� and �Settings�
% by Applying the function �TrackFrame� to the whole video.
%%


idx1 = find(Data.Output.Nose(:,1) > 0 & Data.Output.Nose(:,1) < max(Data.Video.Width,Data.Video.Height));
idx2 = find(Data.Output.Nose(:,2) > 5  & Data.Output.Nose(:,2) <  max(Data.Video.Width,Data.Video.Height));
idx = intersect(idx1,idx2);

%%
tic
Video = Data.Video;
nFrames = length(idx);
perc = round(length(idx)/20);
disp(['Processing ' Data.FileName])
tick = 1;
h = waitbar(0,'Tracking Video');
tic
time(1) = 0;
for id = 1:nFrames
    ETA = (nFrames-id)/(id/time(id))/60;
    
    h.Children.Title.String = sprintf('Tracking Video - #%d/%d @%2.1fFPS ETA:%3.2fmin',id,nFrames,id/time(id),ETA);
    waitbar(id/nFrames,h);
    FrameNR = idx(id);   
    
    Data.SFframe = LoadFrame(Video,FrameNR);
    Data = TrackFrame(Data,Settings);
    Traces{FrameNR} = Data.Traces;
    
    %{
    if rem(FrameNR,perc) == 0
        disp(['   ' num2str(tick*5) '% processed'])
        tick = tick+1;
    end
    %}
    time(id+1) = toc;
end
close(h)
Data.Traces = Traces;
Data.Output.RawTraces = Traces;
Output = Data.Output;
save([Data.PathName Data.FileName(1:end-4) '_Annotations'],'Output')
%toc
nFrames;
    