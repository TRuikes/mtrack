function mTraces = ManualLinking(Data,fig)
%mTraces = ManualLinking(Data,Settings) Shows tracking results and allows
% user to manually label the traces:
% - Press a number 1-9
% - double click a trace in the image to assign label (reassigning
%  possible)
% - Press q to go to next screen
%% Global Variables
video = Data.Video;
Traces = Data.Output.RawTraces;
totframes = size(Traces,2);
cc = hsv(9);
mTraces = cell(1,totframes);
fstart = 1;
matchlog = [];
Data.minlength= '0';
if isfield(Data.Output,'ManTraces') && ~isempty(Data.Output.ManTraces)
    a = inputdlg('Do you want to continue in previous marking? (y/n)');
    while any(class(a{1}) ~= 'char')||  any(size(a{1}) ~= [1,1])
         a = inputdlg('Overwrite existing file?(y/n/b)');
    end
    if a{1} == 'y'
        mTraces = Data.Output.ManTraces;
        for i = size(mTraces,2):-1:1
            if  ~isempty(mTraces{i}) 
                break
            end
        end
        fstart = i;
    elseif a{1} == 'b' 
        mTraces = load('ManTracesBackup.mat');
        mTraces = mTraces.mTraces;
        
        for i = size(mTraces,2):-1:1
            if  ~isempty(mTraces{i}) 
                break
            end
        end
        fstart = i;
    else
        fstart = 1;
    end
end
%% Manual linking

tick = 1;    
for framenr = fstart:totframes
    
    % Load frame, traces and display
    frame = read(video,framenr);
    T = Traces{framenr};
    
    axes(fig)
    cla
    imshow(frame)
    text(10,20,'Keys:','color','y')
    text(10,45,' -(1-9): Label','color','y')
    text(10,70,' -r: Remove Label','color','y')
    text(10,95,' -q: Next frame','color','y')
    text(10,120,' -m: Set minimum length','color','y')
    text(10,145,' -esc: Quit','color','y')
    text(590,15,num2str(framenr),'color','y')
    text(590,40,['/' num2str(totframes)],'color','y')
    
    for i = 1:9
        labels{i} = text(1,1,'','FontSize',12,'FontWeight','bold',...
            'Color',cc(i,:));
    end
    
    if isempty(Traces{framenr}) % If no traces, skip frame
        pause(0.01)
        continue
    end
    
    
    if framenr == 1 || isempty(mTraces{framenr-1}) % No prediction if no previous frames
        
        % Display traces
        hold on
        for i = 1:size(T,2)
            plot(T{i}(:,2),T{i}(:,1),'k')
        end
        
       
        
        % Manual tracing
        [mTraces,quitflag,labels,minlength] = ManualLinkingUserInput(mTraces,Traces,framenr,matchlog,fig,labels);
        
        for IDX = 1:size(mTraces{framenr},2)
            mRotTraces{framenr}{IDX} = RotateTrace(mTraces{framenr}{IDX}... 
                ,Data,framenr);
        end
        
        % Display results with labels
        for c = 1:size(mTraces{framenr},2)
            if ~isempty(mTraces{framenr}{c})
                plot(mTraces{framenr}{c}(:,2),mTraces{framenr}{c}(:,1),...
                    'color',cc(c,:))
                labels{c}.Position = [mTraces{framenr}{c}(end,2)-10,...
                    mTraces{framenr}{c}(end,1),0];
                labels{c}.String = num2str(c);
            end
        end
        
        
        
        
    elseif ~isempty(mTraces{framenr-1})  
        
        % Find number of previous empty frames. Not relevant in this
        % version, but if I want to include speed/acceleration for
        % predition this is needed
        %{
        flag = 0;
        tick = framenr - 1;
        while flag == 0
            if ~isempty(mTraces{tick})
                flag = 0;
                tick = tick-1;
            else
                flag = 1;
            end
        end
        %}
        
        % Make a prediction        
        [mTraces,matchlog,untraced] = ManualLinkingPredictMatch...
            (mTraces,Traces,Data,framenr);
        
        
        axes(fig)
        hold on
        
        
        % Show previous labelled traces
         if ~isempty(mTraces{framenr-1})
            for i = 1:size(mTraces{framenr-1},2)
                trace = mTraces{framenr-1}{i};
                if ~isempty(trace)                    
                   p = plot(trace(:,2),trace(:,1),'color',cc(i,:));
                   p.LineStyle = '-.';
                   p.LineWidth = 0.3;
                   
                
                end
            end
        end
        % Show untraced results
        for c = 1:length(untraced)
            if untraced(c) ~= 0
                plot(Traces{framenr}{untraced(c)}(:,2),...
                    Traces{framenr}{untraced(c)}(:,1),'k')
            end
        end
        % Show labelled results
        for c = 1:size(mTraces{framenr},2)
            if ~isempty(mTraces{framenr}{c})
                plot(mTraces{framenr}{c}(:,2),mTraces{framenr}{c}(:,1),...
                    'color',cc(c,:))
                labels{c}.Position = [mTraces{framenr}{c}(end,2)-10,...
                    mTraces{framenr}{c}(end,1),0];
                labels{c}.String = num2str(c);
            end
        end
        
        % Ask for user adjustments
        [mTraces,quitflag,labels,minlength] = ManualLinkingUserInput(mTraces,Traces,framenr,matchlog,fig,labels);   
        if minlength{1} ~= 999
            Data.minlength = minlength{1};
            
        end
    end
    
    % Reset lables
    for i = 1:9
        labels{i}.String = '';
    end
    
    if quitflag == 1
        break
    end
    tick = tick+1;
    % Save a backup
    if tick == 20
        tick = 1;
        save('ManTracesBackup.mat','mTraces')
    end
        
end

axes(fig)
cla
