function [mTraces,quitflag,labeltemp,minlength] = ManualLinkingUserInput(mTraces,Traces,framenr,matchlog,fig,labeltemp)
% [mTraces] = ManualLinkingUserInput(mTraces,Traces,framenr,matchlog)
%Ask for user input to label traces
%% Global Variables
cc = hsv(9);
for i = 1:9
    names{48+i} = num2str(i);      
end
T = Traces{framenr};
totframes = size(Traces,2);
axes(fig)

in = 0;
minlength{1} = 999;
quitflag = 0;

%% Continuously ask for userinput
while in ~= 113 % @q
    
    if in == 27
        quitflag = 1;
        break
    end
    % ask userinput
    in = getkey;
    if in >=49 && in <= 57 % @1-9
        
        % Select trace name
        name = names{in};        
        
        
        % Mark trace closest to selected point
        [x,y] = getpts(fig);
        Point = [y(1) x(1)];
        dist = [];
        for tidx = 1:size(T,2)
            dist(tidx) = finddist(Point',T{tidx});
        end        
        idx = find(dist == min(dist));
        nr = str2num(name);
        
        
        % Store result and update figure
        if ~isempty(mTraces{framenr}) &&...
               nr <=  size(mTraces{framenr},2) &&...
                ~isempty(mTraces{framenr}{nr})
            overwrite = 1;
        else
            overwrite = 0;
        end
        mTraces{framenr}{nr} = Traces{framenr}{idx};
        matchlog(nr) = idx;
        plot(Traces{framenr}{idx}(:,2),Traces{framenr}{idx}(:,1),...
            'color',cc(nr,:))
        labeltemp{nr}.Position = [Traces{framenr}{idx}(end,2)-10,...
            Traces{framenr}{idx}(end,1),0];
        labeltemp{nr}.String = name;
        
        % Delete previous entry        
        if overwrite == 1         
            id = find(matchlog == idx);
            id(id == nr) = NaN;
            id = id(~isnan(id));
            matchlog(id) = 0;
            
            if ~isempty(id)
                Trace = mTraces{framenr}{id};
                plot(Trace(:,2),Trace(:,1),'k')
                mTraces{framenr}{id} = [];
            end
        end
        
        
    elseif in == 114 % R
        % Remove a trace
        
        [x,y] = getpts(fig);
        
        Point = [y(1) x(1)];
        
        minidx = [];
        for Tidx = 1:size(mTraces{framenr},2)
            dist = [];
            
            if ~isempty(mTraces{framenr}{Tidx})
                for tidx = 1:size(mTraces{framenr},2)
                    dist(tidx) = finddist(Point',mTraces{framenr}{Tidx});
                end
                minidx(Tidx) = dist(find(dist == min(dist),1,'first'));
            else
                minidx(Tidx) = 99;
            end
            
        end
        if any(minidx ~= 99)
            nr = find(minidx == min(minidx),1,'first');
            plot(mTraces{framenr}{nr}(:,2),mTraces{framenr}{nr}(:,1),'color','k')
            labeltemp{nr}.String = ' ';
            mTraces{framenr}{nr} = [];
            
            if ~isempty(matchlog) && nr <= length(matchlog)
                matchlog(nr) = 0;
                
            end
        end
        
    elseif in == 109 % M
        % assign minimumlength
        minlength = inputdlg('Set min length');
                
        
    end
end

