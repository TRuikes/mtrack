function [Trace,TraceState] = FindPoint(Data,Settings,Trace,TraceState)
% [Trace,TraceState] = FindPoint(Data,Settings,Trace,TraceState) finds a 
% next point for intput �Trace�. Its direction of tracing is controlled by 
% TraceState (�ToSnout� or �ToTip�) by prediciting an ROI in the defined 
% direction. Finds a next point using peakdetection and validates the 
% point using �ValidatePoint�. If it is a valid point, it is added to Trace, 
% else TraceState is updated to �ToTip� or �Finished�.
%%
frame = Data.SFframe;
Silhouette = Data.SilhouetteSmall;
Objects = Data.Objects;
SL = Data.Silhouette;
Tracked = Data.Tracked;

LastPointIDX = size(Trace,1);
NewPointIDX = LastPointIDX + 1;
HalfCircle = Settings.TRhalfcirclestart:1:Settings.TRhalfcircleend;
Ref = [0 1];


switch(TraceState)
    case 'ToSnout'        
        if LastPointIDX == 1
            CircleX = ceil(Trace(1,1) + Settings.TRstepsize*sind(1:10:360));
            CircleY = ceil(Trace(1,2) + Settings.TRstepsize*cosd(1:10:360));            
            CircleIDX = sub2ind(size(frame),CircleX,CircleY);
            id = find(SL(CircleIDX) == 1);
            Profile = frame(CircleIDX(id));
            PointIDX = find(Profile == min(Profile),1,'first');           
           if ~isempty(PointIDX)               
            Point(1) = CircleX(id(PointIDX));
            Point(2) = CircleY(id(PointIDX));
            else
                Point = [0,0];
            end
           
            if ValidatePoint(Data,Settings,Point)
                Trace(NewPointIDX,1:2) = Point;
            else
                TraceState = 'ToTip';    
                Trace = flip(Trace,1);
            end   
        elseif LastPointIDX > 1
            Direction = Trace(LastPointIDX,:) - Trace(LastPointIDX-1,:);
            Angle = atan2d(Direction(2),Direction(1)) - ...
                atan2d(Ref(1),Ref(2));
            Theta = HalfCircle - Angle;
            CircleX = ceil(Trace(LastPointIDX,1) + Settings.TRstepsize...
                *sind(Theta));
            CircleY = ceil(Trace(LastPointIDX,2) + Settings.TRstepsize...
                *cosd(Theta));            
            CircleIDX = sub2ind(size(frame),CircleX,CircleY);
            Profile = frame(CircleIDX);
            PointIDX = find(Profile == min(Profile),1,'first');
            if ~isempty(PointIDX)               
            Point(1) = CircleX(PointIDX);
            Point(2) = CircleY(PointIDX);
            else
                Point = [0,0];
            end
            if ValidatePoint(Data,Settings,Point)
                Trace(NewPointIDX,1:2) = Point;
            else
                TraceState = 'ToTip';
                Trace = flip(Trace,1);
            end           
            
        end
        
    case 'ToTip'
        if LastPointIDX ==  1
            CircleX = ceil(Trace(1,1) + Settings.TRstepsize*sind(1:10:360));
            CircleY = ceil(Trace(1,2) + Settings.TRstepsize*cosd(1:10:360));
            CircleIDX = sub2ind(size(frame),CircleX,CircleY);
            id = find(SL(CircleIDX) == 0);
            Profile = frame(id); %#ok<FNDSB>
            PointIDX = find(Profile == min(Profile),1,'first');
            if isempty(PointIDX)
                Point = [0,0];
            else
                Point(1) = CircleX(id(PointIDX));
                Point(2) = CircleY(id(PointIDX));
            end
            if ValidatePoint(Data,Settings,Point)
                Trace(NewPointIDX,1:2) = Point;
            else
                TraceState = 'Finished';
                Trace = flip(Trace,1);
            end
        else
            Direction = Trace(LastPointIDX,:) - Trace(LastPointIDX - 1,:);
            Angle = atan2d(Direction(2),Direction(1)) - ...
                atan2(Ref(2),Ref(1));
            Theta = HalfCircle - Angle;
            CircleX = ceil(Trace(LastPointIDX,1) + Settings.TRstepsize...
                *sind(Theta));
            CircleY = ceil(Trace(LastPointIDX,2) + Settings.TRstepsize...
                *cosd(Theta));
            CircleIDX = sub2ind(size(frame),CircleX,CircleY);
            Profile = frame(CircleIDX);
            PointIDX = find(Profile == min(Profile),1,'first');
            if ~isempty(PointIDX)               
            Point(1) = CircleX(PointIDX);
            Point(2) = CircleY(PointIDX);
            else
                Point = [0,0];
            end
            if  ValidatePoint(Data,Settings,Point)
                Trace(NewPointIDX,1:2) = Point;
            else
                TraceState = 'Finished';                    
            end
        end
    case 'Finished'      
       
        if size(Trace,1) > 11             
            rawAxis = [1:10]';
            fitAxis = [1:15]';%Settings.TRfitlength;
            pXfit = polyfit(rawAxis,Trace(end-11:end-2,1),1);
            pYfit = polyfit(rawAxis,Trace(end-11:end-2,2),1);
            fitTrace(:,1) = polyval(pXfit,fitAxis);
            fitTrace(:,2) = polyval(pYfit,fitAxis);
            fitTrace = round(fitTrace);         
            
            for i = 5:-1:1               
                Point = fitTrace(end+1-i,:);
                if ~any(Point < 1) && ...
                        ValidatePoint(Data,Settings,Point)                    
                    Trace(end+1,:) = fitTrace(end+1-i,:);
                    TraceState = 'ToTip';
                    Settings.Done = 1;
                end
            end
            
            
        end
        
      
end
