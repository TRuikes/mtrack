function Data = TrackFrame(Data,Settings,frame)
%Data = TrackFrame(Data,Settings) returns a field .Traces in Data 
% containing all tracked traces of the current frame using the intensity 
% convergence algorithm:
% -	Expand mouse silhouette
% -	Detect origins using peakdetection
% - Repeatedly apply 'FindPoint' untill all origins have been traced
%% 
if exist('frame','var')
    Data.SFframe = frame;
end
% Return Traces for a single frame
Data.Tracked = zeros(size(Data.SFframe));
Data.Traces = [];
Data.Origins = [];
%% Find Trace origins
Data.Silhouette = zeros(Data.Video.Height,Data.Video.Width);
Data.Silhouette( find(Data.SFframe <= Settings.TRsilhouettethreshold) ) = 1; %#ok<*FNDSB>
Data.Silhouette( find(Data.Objects) ) = 0;
Data.SilhouetteSmall = imerode(Data.Silhouette,strel('diamond',5));
SE = strel('disk',Settings.TRsilhouettedilationsize);
Data.Silhouette = imdilate(Data.SilhouetteSmall,SE);
TraceOriginIDX = find(edge(Data.Silhouette));

% Sort OriginIDX
[T1,T2] = ind2sub(size(Data.SFframe),TraceOriginIDX);
unmarked = 2:length(TraceOriginIDX);
marked = 1;
temp(1,:) = [T1(1),T2(2)];
for j = 1:length(T1)
    dist = sqrt(sum( ([T1(unmarked),T2(unmarked)] - [temp(end,1),temp(end,2)]).^2,2));
    id = find(dist == min(dist));
    temp = [temp;T1(unmarked(id)),T2(unmarked(id))];
    marked = [marked,unmarked(id)];
    unmarked = [unmarked(1:id-1),unmarked(id+1:end)];
end
TraceOriginIDX = sub2ind(size(Data.SFframe),temp(:,1),temp(:,2));
OriginProfile =  Data.SFframe(TraceOriginIDX);
% Lowpass that shit
b = (1/10)*ones(1,10);
a = 1;
LP = filter(b,a,OriginProfile);
OriginProfile = abs(OriginProfile-LP);

if ~isempty(OriginProfile)
[~,OriginsIDX] = findpeaks(OriginProfile,'Threshold',...
    Settings.TRpeakthreshold);
else
    OriginsIDX = [];
end

if ~isempty(OriginsIDX)
    
    
    [Data.Origins(:,1),Data.Origins(:,2)] = ind2sub(size(Data.SFframe),...
        TraceOriginIDX(OriginsIDX));
    
    for idx = 1:size(Data.Origins,1)
        if ~ValidatePoint(Data,Settings,Data.Origins(idx,:))
            Data.Origins(idx,1:2) = NaN;
        end
    end
    
   
    %% Track traces
    % Rank origins based on their intensity values
    id = find(~isnan(Data.Origins(:,1)));
    Data.Origins = Data.Origins(id,:);
    id = sub2ind(size(Data.SFframe),Data.Origins(:,1),Data.Origins(:,2));
    Data.Origins(:,3) = Data.SFframe(id);
    Data.Origins = sortrows(Data.Origins,3);
    %Data.Origins = flip(Data.Origins,1);
      
     
    
    traceidx = 1;
   
    
    for idx = 1:size(Data.Origins,1)
        Trace = Data.Origins(idx,1:2);
        TraceState = 'ToSnout';
        loopflag = 1;
        
        tick = 0;
        while loopflag == 1
           
            [Trace,TraceState] = FindPoint(Data,Settings,Trace,TraceState);
            
            switch(TraceState)
                case 'Finished'
                    loopflag = 0;
                    % In case we want to extend whisker over an object
                    %{ 
                    [Trace,TraceState] = FindPoint(Data,Settings,...
                        Trace,TraceState);
                    switch(TraceState)
                        case 'Finished'
                            loopflag = 0;
                        case 'ToTip'
                            loopflag = 1;
                    end
                    %}
                    
                otherwise
                    loopflag = 1;
                    Data.Tracked(Trace(end,1)-1:Trace(end,1)+1,Trace(end,2)) = 1;
                    Data.Tracked(Trace(end,1),Trace(end,2)-1:Trace(end,2)+1) =1;
                    
            end
            tick = tick+1;
            if tick == 150 % Emergency break
                break
            end
            
        end
        if ValidateTrace(Settings,Trace)
            Data.Traces{traceidx} = Trace;
            traceidx = traceidx+1;
        else
            for i = 1:size(Trace,1)
                Data.Tracked(Trace(i,1)-1:Trace(i,1)+1,Trace(i,2)) = 0;
                Data.Tracked(Trace(i,1),Trace(i,2)-1:Trace(i,2)+1) = 0;
            end
                
        end
        
    end
    
    
end



