# mTrack #
mTrack is a toolbox for whisker tracking in rodents. To run it, download the repository and add the repository path to matlab. Simply run 'mTrack' 
and you are good to go. 
Example videos can be downloaded [here](https://www.dropbox.com/sh/bfvljkkqnxy5rsx/AAA80H5cy2fOubnp392Hzykua?dl=0). An extensive description of mTrack is 
available within the GUI, including a Quickstart guid (see help or resource folder).

mTrack was part of a master's project, described in this [report](https://www.dropbox.com/s/afacph716xnar7t/Report.pdf?dl=0)

For questions, contact T.Ruikes@student.ru.nl.